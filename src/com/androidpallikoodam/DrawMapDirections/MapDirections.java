package com.androidpallikoodam.DrawMapDirections;

import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.widget.Button;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class MapDirections extends MapActivity {
	MapView mv;

	MapController mc;

	Button Save, Cancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mappage);
		MapView mv = (MapView)findViewById(R.id.mvGoogle);
		mv.setBuiltInZoomControls(true);
		MapController mc = mv.getController();
		ArrayList<GeoPoint> all_geo_points = getDirections(10.154929, 76.390316, 10.015861, 76.341867);
		GeoPoint moveTo = all_geo_points.get(0);
		mc.animateTo(moveTo);
		mc.setZoom(12);
		mv.getOverlays().add(new MyOverlay(all_geo_points));
	}

	public class MyOverlay extends Overlay {
		private ArrayList<GeoPoint> all_geo_points;

		public MyOverlay(ArrayList<GeoPoint> allGeoPoints) {
			super();
			this.all_geo_points = allGeoPoints;
		}

		@Override
		public boolean draw(Canvas canvas, MapView mv, boolean shadow, long when) {
			super.draw(canvas, mv, shadow);
			drawPath(mv, canvas);
			return true;
		}

		public void drawPath(MapView mv, Canvas canvas) {
			int xPrev = -1, yPrev = -1, xNow = -1, yNow = -1;
			Paint paint = new Paint();
			paint.setColor(Color.BLUE);
			paint.setStyle(Paint.Style.FILL_AND_STROKE);
			paint.setStrokeWidth(4);
			paint.setAlpha(100);
			if (all_geo_points != null) for (int i = 0; i < all_geo_points.size() - 4; i++) {
				GeoPoint gp = all_geo_points.get(i);
				Point point = new Point();
				mv.getProjection().toPixels(gp, point);
				xNow = point.x;
				yNow = point.y;
				if (xPrev != -1) {
					canvas.drawLine(xPrev, yPrev, xNow, yNow, paint);
				}
				xPrev = xNow;
				yPrev = yNow;
			}
		}
	}

	public static ArrayList<GeoPoint> getDirections(double lat1, double lon1, double lat2, double lon2) {
		String url = "http://maps.googleapis.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2
				+ "&sensor=false&units=metric";
		String tag[] = {"lat", "lng"};
		ArrayList<GeoPoint> list_of_geopoints = new ArrayList<GeoPoint>();
		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpPost httpPost = new HttpPost(url);
			response = httpClient.execute(httpPost, localContext);
			InputStream in = response.getEntity().getContent();
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(in);
			if (doc != null) {
				NodeList nl1, nl2;
				nl1 = doc.getElementsByTagName(tag[0]);
				nl2 = doc.getElementsByTagName(tag[1]);
				if (nl1.getLength() > 0) {
					list_of_geopoints = new ArrayList<GeoPoint>();
					for (int i = 0; i < nl1.getLength(); i++) {
						Node node1 = nl1.item(i);
						Node node2 = nl2.item(i);
						double lat = Double.parseDouble(node1.getTextContent());
						double lng = Double.parseDouble(node2.getTextContent());
						list_of_geopoints.add(new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6)));
					}
				} else {
					// No points found
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list_of_geopoints;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return true;
	}
}
